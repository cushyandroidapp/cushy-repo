package nextbigthing.cushy.activity;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import nextbigthing.cushy.R;
import nextbigthing.cushy.fragment.HomeFragment;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        HomeFragment  mHomeFragment = new HomeFragment();
        ft.replace(R.id.container, mHomeFragment);
        ft.commit();
        getFragmentManager().executePendingTransactions();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
