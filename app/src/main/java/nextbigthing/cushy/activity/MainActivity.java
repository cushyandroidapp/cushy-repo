package nextbigthing.cushy.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import nextbigthing.cushy.font.RobotoTextView;
import nextbigthing.cushy.R;


public class MainActivity extends AppCompatActivity {

    private RobotoTextView mTvSignUp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTvSignUp = (RobotoTextView) findViewById(R.id.tv_Signup);
        mTvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HomeActivity.class));
               // startActivity(new Intent(MainActivity.this, SignupActivity.class));
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
