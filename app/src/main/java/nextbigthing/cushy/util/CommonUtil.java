package nextbigthing.cushy.util;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.widget.TextView;

/**
 * Created by test on 27/02/16.
 */
public class CommonUtil {

    public static class Fonts {
        public static final String REGULAR = "Roboto-Regular.ttf";
        public static final String LIGHT = "Roboto-Light.ttf";
    }

    public static void getStyleAppliedTextView(TextView tv, Context mContext, String fontStyle) {
        String finalString = tv.getText().toString();
        Spannable sBuilder = new SpannableString(finalString);
        Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/" + fontStyle.trim());
        tv.setTypeface(font);
        tv.setText(sBuilder, TextView.BufferType.SPANNABLE);
    }
}
