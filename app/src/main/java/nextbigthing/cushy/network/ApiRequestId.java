package nextbigthing.cushy.network;

/**
 * Created by ujjwal on 04/4/2016.
 */
public enum ApiRequestId {
    DO_SIGN_UP(1);

    private int id;

    ApiRequestId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}

