package nextbigthing.cushy.network;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


/**
 * Created by msomu on 30/04/15.
 * Volley Singleton class for getting requests on priority
 */
public class VolleySingleton {
    private static VolleySingleton sInstance = null;
    //    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;

    private VolleySingleton() {
        mRequestQueue = Volley.newRequestQueue(ShopsUpAppState.getAppContext());
//        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
//
//
//            private LruCache<String, Bitmap> cache = new LruCache<>((int) (Runtime.getRuntime().maxMemory() / 1024) / 8);
//            @Override
//            public Bitmap getBitmap(String url) {
//                return cache.get(url);
//            }
//
//
//            @Override
//            public void putBitmap(String url, Bitmap bitmap) {
//                cache.put(url, bitmap);
//            }
//        });
    }

    public static VolleySingleton getInstance() {
        if (sInstance == null) {
            sInstance = new VolleySingleton();
        }
        return sInstance;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

//    public ImageLoader getImageLoader() {
//        return mImageLoader;
//    }
}