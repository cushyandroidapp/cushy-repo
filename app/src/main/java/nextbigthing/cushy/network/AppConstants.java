package nextbigthing.cushy.network;

import android.net.Uri;

import java.util.HashMap;

/**
 * Created by ujjwal on 12/3/2015.
 */
public class AppConstants {

    // Settings
    //Volley settings
    public static final long DEFAULT_TIMEOUT = 15; // Default timeout in seconds
    public static final int DEFAULT_MAX_RETRIES = 1;
    public static final int GOOGLE_API_CLIENT_TIMEOUT_S = 10; // 10 seconds
    public static final String RESPONSE = "BUNDLE_RESPONSE_KEY";
    public static final String APPLICATION_PACKAGE_NAME = "nextbigthing.cushy";


    public static String BASE_URL;
    public static String HOST_URL;

    public static final String ME = constructBaseURL() + "me";

    public static boolean LIVE_BUILD = false;


    public static String constructBaseURL() {
        if (LIVE_BUILD) {
            AppConstants.HOST_URL = "";
        } else {
            AppConstants.HOST_URL = "";
        }
        return AppConstants.BASE_URL = AppConstants.HOST_URL;
    }

}
