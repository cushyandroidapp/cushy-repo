package nextbigthing.cushy.network;

/**
 * Created by ujjwal on 04/4/2016.
 */
public interface ApiResponseHandler {
    void onSuccess(String requestUrl, ApiRequestId apiRequestId, String response);

    void onFailure(int requestCode, String requestUrl, ApiRequestId apiRequestId, String response);
}
