package nextbigthing.cushy.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import nextbigthing.cushy.R;
import nextbigthing.cushy.adapter.HomeFragmentAdapter;
import nextbigthing.cushy.util.CommonUtil;

public class HomeFragment extends Fragment implements View.OnClickListener{

    private HomeFragmentAdapter mHomeFragmentAdapter;
    private ViewPager mViewPager;
    TextView mAllCushy, mMyCushy, mSavedCushy;
    public static final int ALL_CUSHY = 0;
    public static final int MY_CUSHY = 1;
    public static final int SAVED_CUSHY = 2;

    private ViewPager.OnPageChangeListener listener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            Toast.makeText(getActivity(),"Hi, Page changed: "+ position,Toast.LENGTH_SHORT).show();
            updateTabSelection(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.text_all_cushy:
                resetTheSelection(ALL_CUSHY);
                break;
            case R.id.text_mycushy:
                resetTheSelection(MY_CUSHY);
                break;
            case R.id.text_savedcushy:
                resetTheSelection(SAVED_CUSHY);
                break;
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        mHomeFragmentAdapter = new HomeFragmentAdapter(getChildFragmentManager(), getActivity());
        mViewPager = (ViewPager)view.findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(HomeFragmentAdapter.PAGE_COUNT);
        mViewPager.addOnPageChangeListener(listener);
        mViewPager.setAdapter(mHomeFragmentAdapter);
        mAllCushy = (TextView) view.findViewById(R.id.text_all_cushy);
        mMyCushy = (TextView) view.findViewById(R.id.text_mycushy);
        mSavedCushy = (TextView) view.findViewById(R.id.text_savedcushy);
        mAllCushy.setOnClickListener(this);
        mMyCushy.setOnClickListener(this);
        mSavedCushy.setOnClickListener(this);
        resetTheSelection(ALL_CUSHY);
        return view;

    }

    private void resetTheSelection(int tabPosition) {
        updateTabSelection(tabPosition);
        mViewPager.setCurrentItem(tabPosition, true);
    }

    private void updateTabSelection(int position) {
        deselectTheViews(mAllCushy, mMyCushy, mSavedCushy);
        resetFonts(mAllCushy, mMyCushy, mSavedCushy);
        TextView view = null;

        switch (position) {
            case ALL_CUSHY:
                view = mAllCushy;
                break;

            case MY_CUSHY:
                view = mMyCushy;
                break;

            case SAVED_CUSHY:
                view = mSavedCushy;
                break;
        }
        CommonUtil.getStyleAppliedTextView(view, getActivity(), CommonUtil.Fonts.REGULAR);
        view.setSelected(true);
    }
    public static void deselectTheViews(View... views) {
        for (View v : views) {
            v.setSelected(false);
        }
    }

    private void resetFonts(View... views) {
        for (View view : views) {
            CommonUtil.getStyleAppliedTextView((TextView) view, getActivity(), CommonUtil.Fonts.LIGTHT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
