package nextbigthing.cushy.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import nextbigthing.cushy.fragment.CushyListFragment;


public class HomeFragmentAdapter extends FragmentStatePagerAdapter {
    public static int PAGE_COUNT = 3;
    private CushyListFragment[] mCushyListFragments = new CushyListFragment[3];

    private Context mContext;

    public HomeFragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
        mCushyListFragments[0] = new CushyListFragment();
        mCushyListFragments[1] = new CushyListFragment();
        mCushyListFragments[2] = new CushyListFragment();

    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {

        return mCushyListFragments[position];
    }


}